# ---- Base Node ----
FROM node:12 AS base
ARG COMMIT_REF
ENV COMMIT_REF $COMMIT_REF
USER node:node
WORKDIR /home/node
CMD node index.js
# copy project file
COPY --chown=node:node package*.json .npmrc* yarn* ./
RUN ls -la
RUN cat .npmrc
 
# ---- Production Dependencies ----
FROM base AS dependencies
# install node packages, just production
RUN npm config set strict-ssl false && \
    npm ci --production --verbose

# ---- Production Deploymenbt ----
FROM dependencies AS production
COPY . .

# ---- Development Dependencies ----
FROM dependencies AS development
# install ALL node_modules, including 'devDependencies'
RUN npm install
COPY . .

# When no target is passed we default to production
FROM production AS default
