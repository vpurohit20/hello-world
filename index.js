const express = require('express');
const app = express();
const port = process.env.PORT || 9006

app.get('/', function(req, res) {
  res.sendStatus(200)
});

app.listen(port);
console.log(`Running Express on port ${port}`);
